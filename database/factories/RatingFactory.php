<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Movie;
use App\Rating;
use App\User;
use Faker\Generator as Faker;

$factory->define(Rating::class, function (Faker $faker) {
    return [
        'movie_id' => function () {
            return factory(Movie::class)->create();
        },
        'user_id' => function () {
            return factory(User::class)->create();
        },
        'rating' => $faker->numberBetween('2', '10'),
    ];
});
