<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Movie;
use Faker\Generator as Faker;

$factory->define(Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'summary' => $faker->text(240),
        'imdb_rating' => $faker->numberBetween(1, 10),
        'year' => $faker->year('now'),
    ];
});
