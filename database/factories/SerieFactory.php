<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Series;
use Faker\Generator as Faker;

$factory->define(Series::class, function (Faker $faker) {
    return [
        'title' => $faker->name(),
        'summary' => $faker->sentence(),
    ];
});
