<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Movie;
use App\Review;
use App\User;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create();
        },
        'reviewable_id' => function () {
            return factory(Movie::class)->create();
        },
        'reviewable_type' => "App\Movie",
        "body" => $faker->sentence(),
        "spoiler" => false,
    ];
});
