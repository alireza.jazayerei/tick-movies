<?php

namespace Tests\Unit;

use App\Genre;
use App\Movie;
use App\Rating;
use App\Review;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MovieModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_a_path()
    {
        $movie = factory(Movie::class)->create();
        $this->assertEquals('movies/' . $movie->id, $movie->path());
    }

    /** @test */
    public function it_has_genres()
    {
        $movie = factory(Movie::class)->create();
        $this->assertInstanceOf(Collection::class, $movie->genres);
    }

    /** @test */
    public function it_has_rating()
    {
        $movie = factory(Movie::class)->create();
        $this->assertInstanceOf(Collection::class, $movie->ratings);
    }

    /** @test */
    public function it_has_reviews()
    {
        $movie = factory(Movie::class)->create();
        $this->assertInstanceOf(Collection::class, $movie->reviews);
    }
}
