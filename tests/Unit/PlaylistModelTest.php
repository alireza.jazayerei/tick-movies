<?php

namespace Tests\Unit;

use App\Playlist;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PlaylistModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_owner()
    {
        $playlist = factory(Playlist::class)->create();
        $this->assertInstanceOf(User::class,$playlist->user);
    }

    //todo test for items
}
