<?php

namespace Tests\Unit;

use App\Movie;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_ratings()
    {
        $this->signIn();
        $this->assertInstanceOf(Collection::class, auth()->user()->ratings);
    }

    /** @test */
    public function it_can_say_a_users_rating_to_a_movie()
    {
        $this->withoutExceptionHandling();
        $user = $this->signIn();
        $user->ratings()->create([
            "ratable_id" => $movieId = factory(Movie::class)->create()->id,
            'ratable_type' => "App\Movie",
            "rating" => 7.5,
        ]);

        $this->assertEquals(7.5, $user->rating($movieId));
    }

    /** @test */
    public function it_have_playlists()
    {
        $user = factory(User::class)->create();
        $this->assertInstanceOf(Collection::class, $user->playlists);
    }

    /** @test */
    public function it_has_followers()
    {
        $user = factory(User::class)->create();
        $this->assertInstanceOf(Collection::class, $user->followers);
    }

    /** @test */
    public function it_has_a_path()
    {
        $user = factory(User::class)->create();
        $this->assertEquals("users/" . $user->id, $user->path());
    }
}
