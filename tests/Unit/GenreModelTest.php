<?php

namespace Tests\Unit;

use App\Genre;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GenreModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_a_path()
    {
        $genre = factory(Genre::class)->create();
        $this->assertEquals('genres/' . $genre->name, $genre->path());
    }
}
