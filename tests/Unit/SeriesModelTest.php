<?php

namespace Tests\Unit;

use App\Series;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SeriesModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_a_path()
    {
        $series = factory(Series::class)->create();
        $this->assertEquals('/series/' . $series->id, $series->path());
    }

    /** @test */
    public function it_has_genres()
    {
        $series = factory(Series::class)->create();
        $this->assertInstanceOf(Collection::class, $series->genres);
    }
}
