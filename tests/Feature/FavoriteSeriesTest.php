<?php

namespace Tests\Feature;

use App\Series;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FavoriteSeriesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_users_can_favorite_a_series()
    {
        $this->signIn();
        $series = factory(Series::class)->create();
        $this->post($series->path().'/favorite');
        $this->assertCount(1,$series->fresh()->favorites);
        $this->assertDatabaseHas("favorites",[
            "favorable_id" => $series->id,
            "favorable_type" => "App\Series",
            "user_id" => auth()->id(),
        ]);
    }

    /** @test */
    public function guests_cannot_favorite_movies()
    {
        $series = factory(Series::class)->create();
        $this->post($series->path() . '/favorite')
            ->assertRedirect('/login');
    }

    /** @test */
    public function series_cannot_be_favored_multiple_times()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $series = factory(Series::class)->create();

        $this->post($series->path() . '/favorite');

        $this->assertCount(1,$series->favorites);

        $this->post($series->path() . '/favorite');

        $this->assertCount(1,$series->favorites);
    }

    /** @test */
    public function series_can_be_unfavored()
    {
        $this->signIn();
        $series = factory(Series::class)->create();

        $this->post($series->path() . '/favorite');

        $this->assertCount(1,$series->favorites);

        $this->delete($series->path() . '/favorite');

        $this->assertCount(0,$series->fresh()->favorites);
    }
}
