<?php

namespace Tests\Feature;

use App\Genre;
use App\Movie;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FavoriteGenreTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_users_can_favorite_genres()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $genre = factory(Genre::class)->create();
        $this->post($genre->path() . '/favorite');
        $this->assertCount(1, $genre->favorites);
        $this->assertDatabaseHas("favorites", ['user_id' => auth()->id(), 'favorable_id' => $genre->id, 'favorable_type' => 'App\Genre']);
    }

    /** @test */
    public function guests_cannot_favorite_genres()
    {
        $genre = factory(Genre::class)->create();
        $this->post($genre->path() . '/favorite')->assertRedirect('/login');
    }

    /** @test */
    public function genres_cannot_be_favored_multiple_times()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $genre = factory(Genre::class)->create();

        $this->post($genre->path() . '/favorite');

        $this->assertCount(1, $genre->favorites);

        $this->post($genre->path() . '/favorite');

        $this->assertCount(1, $genre->favorites);
    }

    /** @test */
    public function genres_can_be_unfavored()
    {
        $this->signIn();
        $genre = factory(Genre::class)->create();

        $this->post($genre->path() . '/favorite');

        $this->assertCount(1, $genre->favorites);

        $this->delete($genre->path() . '/favorite');

        $this->assertCount(0, $genre->fresh()->favorites);
    }
}
