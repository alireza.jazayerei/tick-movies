<?php

namespace Tests\Feature;

use App\Movie;
use App\Series;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Facades\Tests\Arranges\UserFactory;
use Tests\TestCase;

class ActivityTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function leaving_a_review_generates_an_activity()
    {
        $movie = factory(Movie::class)->create();
        $john = UserFactory::withFollower($jane = factory(User::class)->create())->create();
        $this->signIn($jane);

        $this->post($movie->path() . "/review", ["body" => "im jane yohooo"]);

        $this->assertDatabaseHas("activities", [
            "user_id" => $jane->id,
            "type" => "created_review",
            "subject_id" => $movie->reviews()->first()->id,
            "subject_type" => "App\Review",
        ]);
        $this->assertCount(1, $john->feed());
    }

    /** @test */
    public function rating_a_movie_generates_activity()
    {
        $movie = factory(Movie::class)->create();
        $john = UserFactory::withFollower($jane = factory(User::class)->create())->create();
        $this->signIn($jane);

        $this->post($movie->path() . "/rate", ["rating" => 8]);

        $this->assertDatabaseHas("activities", [
            "user_id" => $jane->id,
            "type" => "created_rating",
            "subject_id" => $movie->ratings()->first()->id,
            "subject_type" => "App\Rating",
        ]);
    }

    /** @test */
    public function rating_a_series_generates_activity()
    {
        $series = factory(Series::class)->create();
        $john = UserFactory::withFollower($jane = factory(User::class)->create())->create();
        $this->signIn($jane);

        $this->post($series->path() . "/rate", ["rating" => 8]);

        $this->assertDatabaseHas("activities", [
            "user_id" => $jane->id,
            "type" => "created_rating",
            "subject_id" => $series->ratings()->first()->id,
            "subject_type" => "App\Rating",
        ]);
    }

    /** @test */
    public function watching_a_movie_generates_activity()
    {
        $movie = factory(Movie::class)->create();
        $john = UserFactory::withFollower($jane = factory(User::class)->create())->create();
        $this->signIn($jane);

        $this->post('watchList' . '/' . $movie->id);

        $this->assertDatabaseHas("activities", [
            "user_id" => $jane->id,
            "type" => "watchlist_item_added",
            "subject_id" => $movie->id,
            "subject_type" => "App\Movie",
        ]);
    }
}
