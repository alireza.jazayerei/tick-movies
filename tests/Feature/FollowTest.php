<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FollowTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_user_can_follow_others()
    {
        $john = $this->signIn();
        $jane = factory(User::class)->create();
        $this->post($jane->path() . '/follow');
        $this->assertEquals(1, $jane->followers_count);
        $this->assertEquals(0, $john->followers_count);
        $this->assertDatabaseHas("followers", [
            "user_id" => $jane->id,
            "follower_id" => $john->id,
        ]);
    }

    /** @test */
    public function guest_cannot_follow_users()
    {
        $jane = factory(User::class)->create();
        $this->post($jane->path() . '/follow')->assertRedirect('/login');
    }
}
