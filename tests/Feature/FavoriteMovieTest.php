<?php

namespace Tests\Feature;

use App\Movie;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FavoriteMovieTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_users_can_favorite_a_movie()
    {
        $this->signIn();
        $movie = factory(Movie::class)->create();
        $this->post($movie->path() . '/favorite');
        $this->assertCount(1, $movie->favorites);
        $this->assertDatabaseHas("favorites",[
            "user_id" => auth()->id(),
            "favorable_id" => $movie->id,
            "favorable_type" => "App\Movie",
        ]);
    }

    /** @test */
    public function guests_cannot_favorite_movies()
    {
        $movie = factory(Movie::class)->create();
        $this->post($movie->path() . '/favorite')
        ->assertRedirect('/login');
    }

    /** @test */
    public function movies_cannot_be_favored_multiple_times()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $movie = factory(Movie::class)->create();

        $this->post($movie->path() . '/favorite');

        $this->assertCount(1,$movie->favorites);

        $this->post($movie->path() . '/favorite');

        $this->assertCount(1,$movie->favorites);
    }

    /** @test */
    public function movies_can_be_unfavored()
    {
        $this->signIn();
        $movie = factory(Movie::class)->create();

        $this->post($movie->path() . '/favorite');

        $this->assertCount(1,$movie->favorites);

        $this->delete($movie->path() . '/favorite');

        $this->assertCount(0,$movie->fresh()->favorites);
    }
}
