<?php

namespace Tests\Feature;

use App\Movie;
use App\Series;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class WatchListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_users_can_mark_a_movies_as_watched()
    {
        $this->signIn();
        $movie = factory(Movie::class)->create();
        $this->post("/watchList/" . $movie->id);
        $this->assertEquals(1, auth()->user()->watched_movies);
        $this->assertDatabaseHas("watch_list", [
            "user_id" => auth()->id(),
            "item_id" => $movie->id,
            "item_type" => "App\Movie",
        ]);
    }

    /** @test */
    public function guests_cannot_mark_movies_as_watched()
    {
        $movie = factory(Movie::class)->create();
        $this->post("/watchList/" . $movie->id)
            ->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_users_can_mark_a_series_as_watched()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $series = factory(Series::class)->create();
        $this->post("/watchList/" . $series->id);
        $this->assertEquals(1, auth()->user()->watched_series);
        $this->assertDatabaseHas("watch_list", [
            "user_id" => auth()->id(),
            "item_id" => $series->id,
            "item_type" => "App\Series",
        ]);
    }
}
