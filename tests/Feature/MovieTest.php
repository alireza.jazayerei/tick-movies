<?php

namespace Tests\Feature;

use App\Movie;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MovieTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function users_can_see_movies()
    {
        $movie = factory(Movie::class, 5)->create();
        $this->get("/movies")
            ->assertJson([["title" => $movie->first()->title]])
            ->assertJson([4 => ["title" => $movie->last()->title]]);
    }

    /** @test */
    public function users_can_see_a_movie()
    {
        $movie = factory(Movie::class)->create();
        $this->get($movie->path())
            ->assertJson([
                [
                    "title" => $movie->title,
                    "summary" => $movie->summary,
                    "imdb_rating" => $movie->imdb_rating,
                    "year" => $movie->year,
                ]
            ]);
    }
}
