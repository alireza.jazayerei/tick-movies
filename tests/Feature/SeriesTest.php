<?php

namespace Tests\Feature;

use App\Series;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SeriesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function users_can_see_all_series()
    {
        $series = factory(Series::class, 5)->create();
        $this->get('/series')
            ->assertJson([
                0 => ['title' => $series->first()->title],
                2 => ['title' => $series[2]->title],
            ]);
    }

    /** @test */
    public function users_can_see_a_serie()
    {
        $serie = factory(Series::class)->create();
        $this->get($serie->path())
            ->assertJson([
                'title' => $serie->title,
                'summary' => $serie->summary,
            ]);
    }
}
