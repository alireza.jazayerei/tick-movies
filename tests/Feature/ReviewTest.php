<?php

namespace Tests\Feature;

use App\Movie;
use App\Review;
use App\Series;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReviewTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_users_can_leave_review_for_a_movie()
    {
        $this->signIn();
        $movie = factory(Movie::class)->create();

        $this->post($movie->path() . '/review', [
            "body" => "wow this film was realy sth!",
        ]);

        $this->assertCount(1, $movie->reviews);
        $this->assertDatabaseHas("reviews", [
            "user_id" => auth()->id(),
            "reviewable_id" => $movie->id,
            "reviewable_type" => "App\Movie",
            "body" => "wow this film was realy sth!",
            "spoiler" => false,
        ]);
    }

    /** @test */
    public function when_a_review_is_fetched_you_can_see_the_owners_rating_to_movie()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $review = factory(Review::class)->create(["user_id" => $user->id]);
        $user->ratings()->create([
            "ratable_id" => $review->reviewable->id,
            'ratable_type' => "App\Movie",
            "rating" => 8,
        ]);

        $this->get($review->reviewable->path())
            ->assertJson([
                [
                    "reviews" => [
                        [
                            "body" => $review->body,
                            "user_rating" => 8,
                        ]
                    ]
                ]
            ]);
    }

    /** @test */
    public function authenticated_users_can_leave_review_for_a_series()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $series = factory(Series::class)->create();

        $this->post($series->path() . '/review', [
            "body" => "wow this tv show was really sth!",
        ]);

        $this->assertCount(1, $series->reviews);
        $this->assertDatabaseHas("reviews", [
            "user_id" => auth()->id(),
            "reviewable_id" => $series->id,
            "reviewable_type" => "App\Series",
            "body" => "wow this tv show was really sth!",
            "spoiler" => false,
        ]);
    }
}
