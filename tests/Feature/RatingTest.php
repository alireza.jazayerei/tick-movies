<?php

namespace Tests\Feature;

use App\Movie;
use App\Series;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RatingTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_users_can_rate_a_movie()
    {
        $this->signIn();
        $movie = factory(Movie::class)->create();

        $this->post($movie->path() . '/rate', ['rating' => 8]);

        $this->assertEquals(8, $movie->fresh()->user_rating);

        $this->signIn();

        $this->post($movie->path() . '/rate', ['rating' => 7]);

        $this->assertEquals(7.5, $movie->fresh()->user_rating);
    }

    /** @test */
    public function guests_cannot_rate_movies()
    {
        $movie = factory(Movie::class)->create();
        $this->post($movie->path() . '/rate')->assertRedirect('/login');
        $this->assertEquals(null, $movie->fresh()->user_rating);
    }

    /** @test */
    public function authenticated_users_can_see_their_rate_to_a_movie()
    {
        $this->signIn();
        $movie = factory(Movie::class)->create();
        $this->post($movie->path() . '/rate', ['rating' => 8]);
        $this->assertEquals(8, auth()->user()->rating($movie->id));
    }

    /** @test */
    public function rating_should_be_between_zero_and_ten()
    {
        $this->signIn();
        $movie = factory(Movie::class)->create();
        $series = factory(Series::class)->create();

        $this->post($movie->path() . '/rate', [])
            ->assertSessionHasErrors(["rating"]);

        $this->post($movie->path() . '/rate', ['rating' => "fegr"])
            ->assertSessionHasErrors(["rating"]);

        $this->post($movie->path() . '/rate', ['rating' => 18])
            ->assertSessionHasErrors(["rating"]);


        $this->post($series->path() . '/rate', [])
            ->assertSessionHasErrors(["rating"]);

        $this->post($series->path() . '/rate', ['rating' => "fegr"])
            ->assertSessionHasErrors(["rating"]);

        $this->post($series->path() . '/rate', ['rating' => 18])
            ->assertSessionHasErrors(["rating"]);
    }

    /** @test */
    public function authenticated_users_can_rate_a_series()
    {
        $this->withoutExceptionHandling();
        $this->signIn();
        $series = factory(Series::class)->create();

        $this->post($series->path() . '/rate', ['rating' => 8]);

        $this->assertEquals(8, $series->fresh()->user_rating);

        $this->signIn();

        $this->post($series->path() . '/rate', ['rating' => 7]);

        $this->assertEquals(7.5, $series->fresh()->user_rating);
    }

    /** @test */
    public function guests_cannot_rate_series()
    {
        $series = factory(Series::class)->create();
        $this->post($series->path() . '/rate')->assertRedirect('/login');
        $this->assertEquals(null, $series->fresh()->user_rating);
    }

    /** @test */
    public function authenticated_users_can_see_their_rate_to_a_series()
    {
        $this->signIn();
        $series = factory(Series::class)->create();
        $this->post($series->path() . '/rate', ['rating' => 8]);
        $this->assertEquals(8, auth()->user()->rating($series->id));
    }
}
