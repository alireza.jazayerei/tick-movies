<?php

namespace Tests\Feature;

use App\Genre;
use App\Movie;
use App\Series;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Facades\Tests\Arranges\GenreFactory;
use Tests\TestCase;

class GenreTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function users_can_see_all_movies_in_a_genre()
    {
        $genre = GenreFactory::withMovies(2)->create();
        $this->get("movies/" . $genre->path())
            ->assertJson([["title" => $genre->movies->first()->title]])
            ->assertJson([1 => ["title" => $genre->movies->last()->title]]);
    }

    /** @test */
    public function users_can_see_all_series_in_a_genre()
    {
        $this->withoutExceptionHandling();
        $genre = GenreFactory::withSeries(2)->create();
        $this->get("series/" . $genre->path())
            ->assertJson([["title" => $genre->series->first()->title]])
            ->assertJson([1 => ["title" => $genre->series->last()->title]]);
    }
}
