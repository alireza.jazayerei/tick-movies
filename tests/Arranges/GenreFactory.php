<?php

namespace Tests\Arranges;

use App\Genre;
use App\Movie;
use App\Series;

class GenreFactory
{
    private $movieCount = 0;
    private $seriesCount = 0;

    public function create()
    {
        $genre = factory(Genre::class)->create();

        $genre->movies()->attach(factory(Movie::class, $this->movieCount)->create()->pluck("id"));

        $genre->series()->attach(factory(Series::class, $this->seriesCount)->create()->pluck("id"));

        return $genre;

    }

    public function withMovies($count = 1)
    {
        $this->movieCount = $count;
        return $this;
    }

    public function withSeries($count = 1)
    {
        $this->seriesCount = $count;
        return $this;
    }
}
