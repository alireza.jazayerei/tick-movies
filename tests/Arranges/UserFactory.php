<?php


namespace Tests\Arranges;


use App\User;

class UserFactory
{
    private $follower;

    public function create()
    {
        $user = factory(User::class)->create();
        if ($this->follower) {
            $user->followers()->attach($this->follower->id);
        }
        return $user;
    }

    public function withFollower($person)
    {
        $this->follower = $person;
        return $this;
    }
}
