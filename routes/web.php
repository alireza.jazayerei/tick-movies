<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Playlist;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/movies', 'MovieController@index');
Route::get('/movies/{movie}', 'MovieController@show');

Route::get('movies/genres/{genre}', "MovieGenreController@show");

Route::get('series/genres/{genre}', "SeriesGenreController@show");

Route::post('/genres/{genre}/favorite', "FavoriteGenreController@store")->middleware('auth');
Route::delete('/genres/{genre}/favorite', "FavoriteGenreController@destroy")->middleware('auth');

Route::post('/movies/{movie}/rate', "MovieRatingController@store")->middleware('auth');

Route::post('/movies/{movie}/favorite', "FavoriteMovieController@store")->middleware('auth');
Route::delete('/movies/{movie}/favorite', "FavoriteMovieController@destroy")->middleware('auth');

Route::post('/movies/{movie}/review', "ReviewMovieController@store")->middleware('auth');

Route::post('/playlists', 'PlaylistController@store')->middleware('auth');

Route::post('playlists/{playlist}/{itemId}', 'PlaylistItemController@store')->middleware('auth');
Route::delete('playlists/{playlist}/{itemId}', 'PlaylistItemController@destroy')->middleware('auth');

Route::post('/watchList/{itemId}', 'WatchListController@store')->middleware('auth');

Route::get('/series', 'SeriesController@index');
Route::get('/series/{series}', 'SeriesController@show');

Route::post('/series/{series}/favorite', 'FavoriteSeriesController@store')->middleware('auth');
Route::delete('/series/{series}/favorite', 'FavoriteSeriesController@destroy')->middleware('auth');

Route::post('/series/{series}/rate', 'SeriesRatingController@store')->middleware('auth');

Route::post('/series/{series}/review', "ReviewSeriesController@store")->middleware('auth');

Route::post('/users/{user}/follow','FollowerController@store')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
