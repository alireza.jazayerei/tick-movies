<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, ManagesItem;

    protected static $itemTypes = [Movie::class, Series::class];

    protected $itemTable = 'watch_list';

    protected $appends = ['watched_movies', 'watched_series'];


    protected $fillable = [
        'name', 'email', 'password',
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function rating($movieId)
    {

        return $this->ratings()->where('ratable_id', $movieId)->first(['rating'])->rating;
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function playlists()
    {
        return $this->hasMany(Playlist::class);
    }

    public function getWatchedMoviesAttribute()
    {
        return $this->movies()->count();
    }

    public function getWatchedSeriesAttribute()
    {
        return $this->series()->count();
    }

    public function getFollowersCountAttribute()
    {
        return $this->followers()->count();
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, "followers", "user_id", "follower_id");
    }

    public function path()
    {
        return "users/" . $this->id;
    }

    public function feed()
    {
        return Activity::with('subject')
            ->whereIn('user_id', $this->followers()->pluck('id'))
            ->latest()
            ->get();
    }
}
