<?php

namespace App;

use App\Providers\MovieRated;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use RecordsActivity;

    protected static $activityEvents = ['created'];

    protected $fillable = ['ratable_id', 'rating', 'ratable_type', 'user_id'];

    public function movie()
    {
        return $this->morphedByMany(Movie::class, 'ratable', 'ratings');
    }

    public function series()
    {
        return $this->morphedByMany(Movie::class, 'ratable', 'ratings');
    }
}
