<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use Favorable;

    public function movies()
    {
        return $this->morphedByMany(Movie::class,'genreable','genreables');
    }

    public function series()
    {
        return $this->morphedByMany(Series::class,'genreable','genreables');

    }

    public function path()
    {
        return 'genres/' . $this->name;
    }

    public function getRouteKeyName()
    {
        return 'name';
    }

}
