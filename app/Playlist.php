<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    use ManagesItem;

    protected $fillable = ['title','user_id'];

    protected $itemTable = 'playlist_items';

    protected static $itemTypes = [Movie::class, Series::class];

    public function path()
    {
        return '/playlists/' . $this->id;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
