<?php


namespace App;

use App\Providers\ItemAdded;
use Illuminate\Support\Str;

trait ManagesItem
{
    private static $relationMethods = [];

    public static function bootManagesItem()
    {
        foreach (self::$itemTypes as $type) {
            self::$relationMethods[] = self::getRelationName($type);
        }
    }

    private static function getRelationName($type)
    {
        return strtolower(Str::plural(class_basename($type)));
    }

    public function items()
    {
        $counter = 0;
        foreach (self::$relationMethods as $method) {
            if ($this->$method()->get()->isNotEmpty()) {
                $counter === 0 ?
                    $items = $this->$method()->get() : $items->push($this->$method()->get());
                $counter++;
            }
        }
        return $items ?? null;
    }

    public function addItem($itemId)
    {
        foreach (self::$itemTypes as $type) {
            if ($type::where('id', $itemId)->count() === 1) {
                $relation = self::getRelationName($type);
                $this->$relation()->attach($itemId);
                event( new ItemAdded($this,$type,$itemId));
            }
        }
    }

    public function removeItem($itemId)
    {
        foreach (self::$itemTypes as $type) {
            if ($type::where('id', $itemId)->count() === 1) {
                $relation = self::getRelationName($type);
                $this->$relation()->detach($itemId);
            }
        }
    }

    public function __call($name, $arguments)
    {
        if (in_array($name, self::$relationMethods)) {
            return $this->morphedByMany("App\\" . Str::camel(Str::singular($name)), 'item', $this->itemTable);
        }
        return parent::__call($name, $arguments);
    }
}
