<?php

namespace App\Policies;

use App\Playlist;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PlaylistPolicy
{
    use HandlesAuthorization;

    public function update(User $user,Playlist $playlist)
    {
        return $user->is($playlist->user);
    }
}
