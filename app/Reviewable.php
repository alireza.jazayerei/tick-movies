<?php


namespace App;


trait Reviewable
{
    public function reviews()
    {
        return $this->morphMany(Review::class, 'reviewable');
    }

    public function addReview($body,$spoiler)
    {
        $this->reviews()->create([
        "user_id" => auth()->id(),
        "body" => $body,
        "spoiler" => $spoiler,
    ]);
    }
}
