<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use RecordsActivity;

    protected static $activityEvents = ['created'];

    protected $fillable = ['user_id', "body", "spoiler"];

    protected $appends = ['user_rating'];

    public function reviewable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUserRatingAttribute()
    {
        return $this->user->rating($this->reviewable_id);
    }
}
