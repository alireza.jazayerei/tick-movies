<?php

namespace App\Providers;

use App\Activity;
use App\Providers\ItemAdded;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RecordActivity
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ItemAdded $event
     * @return void
     */
    public function handle(ItemAdded $event)
    {
        if (! auth()->check()) return;
        Activity::create([
            "user_id" => auth()->id(),
            "type" => class_basename($event->subject) === "User" ? 'watchlist_item_added' : strtolower(class_basename($event->subject)) . '_item_added',
            "subject_id" => $event->itemId,
            'subject_type' => $event->type,
        ]);
    }
}
