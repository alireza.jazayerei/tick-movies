<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Playlist;
use Illuminate\Http\Request;

class PlaylistItemController extends Controller
{
    public function store(Playlist $playlist, $itemId)
    {
        $this->authorize('update', $playlist);
        $playlist->addItem($itemId);
    }

    public function destroy(Playlist $playlist, $itemId)
    {
        $this->authorize('update', $playlist);
        $playlist->removeItem($itemId);
    }
}
