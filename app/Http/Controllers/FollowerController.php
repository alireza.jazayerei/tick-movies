<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FollowerController extends Controller
{
    public function store(User $user)
    {
        $user->followers()->attach(["follower_id" => auth()->id()]);
    }
}
