<?php

namespace App\Http\Controllers;

use App\Series;
use Illuminate\Http\Request;

class ReviewSeriesController extends Controller
{
    public function store(Request $request, Series $series)
    {
        $series->addReview($request->input('body'), $request->has('spoiler'));
    }
}
