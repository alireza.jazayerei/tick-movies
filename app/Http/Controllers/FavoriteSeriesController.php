<?php

namespace App\Http\Controllers;

use App\Series;


class FavoriteSeriesController extends Controller
{
    public function store(Series $series)
    {
       $series->favorite();
    }

    public function destroy(Series $series)
    {
        $series->unfavorite();
    }
}
