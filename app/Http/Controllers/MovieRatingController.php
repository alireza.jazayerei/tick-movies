<?php

namespace App\Http\Controllers;

use App\Http\Requests\RatingRequest;
use App\Movie;
use App\Rating;
use Illuminate\Http\Request;

class MovieRatingController extends Controller
{
    public function store(RatingRequest $request, Movie $movie)
    {
        $movie->rate($request->input('rating'));
    }
}
