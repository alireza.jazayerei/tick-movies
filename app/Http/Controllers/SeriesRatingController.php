<?php

namespace App\Http\Controllers;

use App\Http\Requests\RatingRequest;
use App\Series;

class SeriesRatingController extends Controller
{
    public function store(RatingRequest $request, Series $series)
    {
        $series->rate($request->input('rating'));
    }
}
