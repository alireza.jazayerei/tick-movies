<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;

class SeriesGenreController extends Controller
{
    public function show(Genre $genre)
    {
        return $genre->series;
    }
}
