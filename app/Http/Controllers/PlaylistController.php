<?php

namespace App\Http\Controllers;

use App\Playlist;
use Illuminate\Http\Request;

class PlaylistController extends Controller
{
    public function store(Request $request)
    {
        auth()->user()->playlists()->create([
            'title' => $request->input('title'),
        ]);
    }
}
