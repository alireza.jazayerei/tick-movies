<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;

class FavoriteMovieController extends Controller
{
    public function store(Movie $movie)
    {
        $movie->favorite();
    }

    public function destroy(Movie $movie)
    {
        $movie->unfavorite();
    }
}
