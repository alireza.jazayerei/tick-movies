<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;

class ReviewMovieController extends Controller
{
    public function store(Request $request, Movie $movie)
    {
        $movie->addReview($request->input('body'),$request->has('spoiler'));
    }
}
