<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WatchListController extends Controller
{
    public function store($itemId)
    {
        auth()->user()->addItem($itemId);
    }
}
