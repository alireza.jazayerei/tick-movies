<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;

class FavoriteGenreController extends Controller
{
    public function store(Genre $genre)
    {
        $genre->favorite();
    }

    public function destroy(Genre $genre)
    {
        $genre->unfavorite();
    }
}
