<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use Favorable, Ratable, Reviewable;

    protected $appends = ['user_rating'];

    public function path()
    {
        return "movies/" . $this->id;
    }

    public function genres()
    {
        return $this->morphToMany(Genre::class,'genreable','genreables');
    }
}
