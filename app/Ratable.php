<?php


namespace App;


trait Ratable
{
    public function rate($rating)
    {
        $this->ratings()->create([
            'rating' => $rating,
            'user_id' => auth()->id(),
        ]);
    }

    public function ratings()
    {
        return $this->morphMany(Rating::class, 'ratable');
    }

    public function getUserRatingAttribute()
    {
        return $this->ratings()->average('rating');
    }
}
