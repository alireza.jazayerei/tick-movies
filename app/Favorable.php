<?php


namespace App;


trait Favorable
{
    public function favorites()
    {
        return $this->morphMany(Favorite::class,'favorable');
    }

    public function favorite()
    {
        if (! $this->favorites()->where('user_id',auth()->id())->exists()) {
            $this->favorites()->create([
                "user_id" => auth()->id(),
            ]);
        }
    }

    public function unfavorite()
    {
        $this->favorites()->where('user_id',auth()->id())->delete();
    }

}
