<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    use Favorable, Ratable, Reviewable;

    public function path()
    {
        return '/series/' . $this->id;
    }

    public function genres()
    {
        return $this->morphToMany(Genre::class,'genreable','genreables');
    }
}
